# ElX

elX is a window manager for X written in Elixir. It is currently under construction.

## Compiling elX

To install elX, you will need [Elixir](http://elixir-lang.org/install.html)

Clone the elX repository in the location of your choice and `cd` to it, then run:
```
mix deps.get
mix deps.compile
mix escript.build
```

## Using elX

At the moment, elX is not in a usable state.

## Todo

### Short term

- [ ] Type specs and dialyze ?
- [ ] Actual window management functions
  - [X] Substructure redirection and handling on root window
  - [X] Window layout policy -> container system
  - [X] Container behaviour and default functions
  - [X] Keyboard/mouse user interaction
- [ ] Make the whole program more OTP.
  - [X] Split into an X11 application, a WM application, maybe a UI application?
  - [ ] Maybe `WM.HotKeyDaemon` could be made into a GenStateMachine?
  - [X] Better communication with supervisors
- [X] Ignore NumLock/CapsLock/ScrollLock/etc. in hotkeys
- [x] Mutual adoption by containers, ie. automatically set the parent of a container when adding it as a child and vice versa.
- [X] Make Container functions synchronous -> by using sys and proc_lib?
- [ ] Children reordering in Containers
- [ ] Allow the position to be specified when adding a child to a container.
- [X] WM.Manager should be more OTP (GenStateMachine ?)
- [ ] How to handle window stacking oder ? With a specific container ?
- [ ] How to handle moving a window from a container to another ?
- Types of container:
  - [X] Bottom-level (represents a window)
  - [X] Top-level (root of the container tree)
  - [X] Split (tiling)
  - [ ] Multiplex
  - [ ] Frame -> Reparenting window ?
  - [ ] Application container containing a window controled by ElX
  - [ ] Stacking

- [X] Configuration by loading a user-defined exs script that defines a "Config" module ?
  - [ ] Module system -> Each module describes an interaction mode with its own hotkeys, hooks, etc. and the whole WM is a finite state machine where each module is a mode (aka a state in the state machine).
	- [ ] Have a 'global state' variable that can be shared between modes ?
  - [ ] DSL
    - [X] Nice way to describe keystrokes ? With a macro ?
	- [ ] Some way to run shell commands -> integrate Porcelain ?
	- [ ] Window and Container manipulation primitives -> Hardest part...
	- [ ] Hooks into Container functions
	- [ ] Hooks into X11 events (when the cursor enters a window, for instance)
	- [ ] Graphics primitives? Include them in XEB?
	- [X] Make it possible to not return `{:ok, state}` in every piece of code in the module...
	- [ ] Possibility for another process (eg. one that draws a bar) to access Container hooks

- [ ] Multiple monitors support -> Xinerama ?

- [ ] Optimize for memory usage.
	- [ ] Seriously. It's getting bad.

- [ ] More documentation. Very important.

### Long term objectives

- [X] Split the X11 module into a separate project. _(see XEB)_
- [ ] ICCCM/EWMH support
- [ ] Basic graphics -> Maybe do that in XEB or in another project?

## License

The entirety of the code is under MIT license.


