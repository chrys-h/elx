defmodule ElX.Mixfile do
  use Mix.Project

  def project do
    [app: :elX,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps(),
     escript: escript()]
  end

  def application do
    [applications: [:logger, :xeb],
     mod: {ElX, []}]
  end

  defp deps do
    [{:xeb, github: "chrys-h/XEB", submodules: true},
     {:gen_state_machine, "~> 2.0"}]
  end

  defp escript do
    [main_module: ElX]
  end
end
