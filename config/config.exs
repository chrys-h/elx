use Mix.Config

config :logger,
  backends: [:console],
  compile_time_purge_level: :warn,
  level: :warn

config :xeb,
  extensions: {:only, [:xproto]}

config :elX,
  userconfig_path: "~/.config/elx/elxrc.exs"

