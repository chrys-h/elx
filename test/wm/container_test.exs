defmodule WM.ContainerTest do
  use ExUnit.Case

  test "Stops on command" do
    {:ok, cont} = WM.Containers.Blank.start_link()

    WM.Container.stop cont

    refute Process.alive?(cont)
  end

  test "Adopts new children on command" do
    {:ok, parent} = WM.Containers.Blank.start_link()
    {:ok, child} = WM.Containers.Blank.start_link()

    WM.Container.add_child parent, child

    assert WM.Container.get_state(parent, :c_children) == [child]
    assert WM.Container.get_state(child, :c_parent) == parent

    WM.Container.stop child
    WM.Container.stop parent
  end

  test "Adopts new parent on command" do
    {:ok, parent} = WM.Containers.Blank.start_link()
    {:ok, child} = WM.Containers.Blank.start_link()

    WM.Container.set_parent child, parent
    :timer.sleep(100)

    assert WM.Container.get_state(parent, :c_children) == [child]
    assert WM.Container.get_state(child, :c_parent) == parent

    WM.Container.stop child
    WM.Container.stop parent
  end

  test "Forgets child on command" do
    {:ok, child} = WM.Containers.Blank.start_link()
    {:ok, other_child} = WM.Containers.Blank.start_link()
    {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

    assert WM.Container.get_state(parent, :c_children) == [child, other_child]
    assert WM.Container.get_state(child, :c_parent) == parent
    assert WM.Container.get_state(other_child, :c_parent) == parent

    WM.Container.remove_child parent, child
    :timer.sleep(100)

    assert WM.Container.get_state(parent, :c_children) == [other_child]
    assert WM.Container.get_state(child, :c_parent) == nil

    WM.Container.stop child
    WM.Container.stop other_child
    WM.Container.stop parent
  end

  test "Forget parent on command" do
    {:ok, child} = WM.Containers.Blank.start_link()
    {:ok, other_child} = WM.Containers.Blank.start_link()
    {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

    assert WM.Container.get_state(parent, :c_children) == [child, other_child]
    assert WM.Container.get_state(child, :c_parent) == parent
    assert WM.Container.get_state(other_child, :c_parent) == parent

    WM.Container.set_parent child, nil
    :timer.sleep(100)

    assert WM.Container.get_state(parent, :c_children) == [other_child]
    assert WM.Container.get_state(child, :c_parent) == nil

    WM.Container.stop child
    WM.Container.stop other_child
    WM.Container.stop parent
  end

  test "Forget child upon death" do
    {:ok, child} = WM.Containers.Blank.start_link()
    {:ok, other_child} = WM.Containers.Blank.start_link()
    {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

    assert WM.Container.get_state(parent, :c_children) == [child, other_child]
    assert WM.Container.get_state(child, :c_parent) == parent
    assert WM.Container.get_state(other_child, :c_parent) == parent

    WM.Container.stop child
    :timer.sleep(100)

    refute Process.alive?(child)
    assert WM.Container.get_state(parent, :c_children) == [other_child]

    WM.Container.stop other_child
    WM.Container.stop parent
  end

  test "Dies in case of parent death" do
    {:ok, child} = WM.Containers.Blank.start_link()
    {:ok, parent} = WM.Containers.Blank.start_link(children: [child])

    assert WM.Container.get_state(parent, :c_children) == [child]
    assert WM.Container.get_state(child, :c_parent) == parent

    WM.Container.stop parent
    :timer.sleep(100)

    refute Process.alive?(child)
  end

  describe "Geometry changes are correctly applied" do
    test "for `nil` fields" do
      base_geom = %WM.Container.Geometry{
        x: 0, y: 0,
        width: 100, height: 100,
        mapped: false
      }

      geom_change = %WM.Container.Geometry{
        x: nil, y: nil,
        width: nil, height: nil,
        mapped: nil
      }

      {:ok, cont} = WM.Containers.Blank.start_link(geometry: base_geom)
      WM.Container.set_geometry cont, geom_change
      assert WM.Container.get_state(cont, :c_geometry) == base_geom

      WM.Container.stop cont
    end

    test "for `:mod` fields" do
      base_geom = %WM.Container.Geometry{
        x: 0, y: 0,
        width: 100, height: 100,
        mapped: false
      }

      geom_change = %WM.Container.Geometry{
        x: {:mod, 10}, y: {:mod, 10},
        width: {:mod, -10}, height: {:mod, -10},
        mapped: nil
      }

      final_geom = %WM.Container.Geometry{
        x: 10, y: 10,
        width: 90, height: 90,
        mapped: false
      }

      {:ok, cont} = WM.Containers.Blank.start_link(geometry: base_geom)
      WM.Container.set_geometry cont, geom_change
      assert WM.Container.get_state(cont, :c_geometry) == final_geom

      WM.Container.stop cont
    end

    test "for `:toggle` fields" do
      base_geom = %WM.Container.Geometry{
        x: 0, y: 0,
        width: 100, height: 100,
        mapped: false
      }

      geom_change = %WM.Container.Geometry{
        x: nil, y: nil,
        width: nil, height: nil,
        mapped: :toggle
      }

      final_geom = %WM.Container.Geometry{
        x: 0, y: 0,
        width: 100, height: 100,
        mapped: true
      }

      {:ok, cont} = WM.Containers.Blank.start_link(geometry: base_geom)
      WM.Container.set_geometry cont, geom_change
      assert WM.Container.get_state(cont, :c_geometry) == final_geom

      WM.Container.stop cont
    end

    test "for literal fields" do
      base_geom = %WM.Container.Geometry{
        x: 0, y: 0,
        width: 100, height: 100,
        mapped: false
      }

      geom_change = %WM.Container.Geometry{
        x: 100, y: 100,
        width: 50, height: 50,
        mapped: true
      }

      {:ok, cont} = WM.Containers.Blank.start_link(geometry: base_geom)
      WM.Container.set_geometry cont, geom_change
      assert WM.Container.get_state(cont, :c_geometry) == geom_change

      WM.Container.stop cont
    end
  end

  test "Propagates geometry changes to children" do
    base_geom = %WM.Container.Geometry{
      x: 0, y: 0,
      width: 100, height: 100,
      mapped: false
    }

    geom_change = %WM.Container.Geometry{
      x: 100, y: 100,
      width: 50, height: 50,
      mapped: true
    }

    {:ok, child} = WM.Containers.Blank.start_link(geometry: base_geom)
    {:ok, parent} = WM.Containers.Blank.start_link(children: [child], geometry: base_geom)

    assert WM.Container.get_state(parent, :c_children) == [child]
    assert WM.Container.get_state(child, :c_parent) == parent

    WM.Container.set_geometry parent, geom_change

    assert WM.Container.get_state(parent, :c_geometry) == geom_change
    assert WM.Container.get_state(child, :c_geometry) == geom_change

    WM.Container.stop child
    WM.Container.stop parent
  end

  describe "Focus movement" do
    test "works for literal integers" do
      {:ok, child} = WM.Containers.Blank.start_link()
      {:ok, other_child} = WM.Containers.Blank.start_link()
      {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

      assert WM.Container.get_state(parent, :c_children) == [child, other_child]
      assert WM.Container.get_state(child, :c_parent) == parent
      assert WM.Container.get_state(other_child, :c_parent) == parent
      assert WM.Container.get_state(parent, :c_focused_child) == 0

      WM.Container.move_focus(parent, 1)

      assert WM.Container.get_state(parent, :c_focused_child) == 1

      WM.Container.stop child
      WM.Container.stop other_child
      WM.Container.stop parent
    end

    test "is ignored for out-of-bounds integers" do
      {:ok, child} = WM.Containers.Blank.start_link()
      {:ok, other_child} = WM.Containers.Blank.start_link()
      {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

      assert WM.Container.get_state(parent, :c_children) == [child, other_child]
      assert WM.Container.get_state(child, :c_parent) == parent
      assert WM.Container.get_state(other_child, :c_parent) == parent
      assert WM.Container.get_state(parent, :c_focused_child) == 0

      WM.Container.move_focus(parent, 6)

      assert WM.Container.get_state(parent, :c_focused_child) == 0

      WM.Container.stop child
      WM.Container.stop other_child
      WM.Container.stop parent
    end

    test "works for `:next`" do
      {:ok, child} = WM.Containers.Blank.start_link()
      {:ok, other_child} = WM.Containers.Blank.start_link()
      {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

      assert WM.Container.get_state(parent, :c_children) == [child, other_child]
      assert WM.Container.get_state(child, :c_parent) == parent
      assert WM.Container.get_state(other_child, :c_parent) == parent
      assert WM.Container.get_state(parent, :c_focused_child) == 0

      WM.Container.move_focus(parent, :next)

      assert WM.Container.get_state(parent, :c_focused_child) == 1

      WM.Container.stop child
      WM.Container.stop other_child
      WM.Container.stop parent
    end

    test "works for `:previous`" do
      {:ok, child} = WM.Containers.Blank.start_link()
      {:ok, other_child} = WM.Containers.Blank.start_link()
      {:ok, parent} = WM.Containers.Blank.start_link(children: [child, other_child])

      assert WM.Container.get_state(parent, :c_children) == [child, other_child]
      assert WM.Container.get_state(child, :c_parent) == parent
      assert WM.Container.get_state(other_child, :c_parent) == parent
      assert WM.Container.get_state(parent, :c_focused_child) == 0

      WM.Container.move_focus(parent, :previous)

      assert WM.Container.get_state(parent, :c_focused_child) == 1

      WM.Container.stop child
      WM.Container.stop other_child
      WM.Container.stop parent
    end

    test "is propagated to parent for unrecognized specifications" do
      {:ok, child} = WM.Containers.Blank.start_link()
      {:ok, watcher} = WM.Containers.Relay.start_link children: [child], report_to: self()
      assert_received {:RC_initialized, ^watcher}

      assert WM.Container.get_state(watcher, :c_children) == [child]
      assert WM.Container.get_state(child, :c_parent) == watcher

      WM.Container.move_focus(child, :left)
      :timer.sleep(100)

      assert_received {:RC_got_move_focus, ^watcher, :left}

      WM.Container.stop child
      WM.Container.stop watcher
    end
  end

  test "Geometry orders are propagated to parent" do
    geom_change = %WM.Container.Geometry{
      x: nil, y: nil,
      width: nil, height: nil,
      mapped: :toggle
    }

    {:ok, child} = WM.Containers.Blank.start_link()
    {:ok, watcher} = WM.Containers.Relay.start_link children: [child], report_to: self()
    assert_received {:RC_initialized, ^watcher}

    assert WM.Container.get_state(watcher, :c_children) == [child]
    assert WM.Container.get_state(child, :c_parent) == watcher
    child_index = 0

    WM.Container.hint_geometry(child, geom_change)
    :timer.sleep(100)

    assert_received {:RC_got_geometry_order, ^watcher, {^child_index, ^geom_change}}

    WM.Container.stop child
    WM.Container.stop watcher
  end

  test "Focus orders are propagated to parent" do
    {:ok, child} = WM.Containers.Blank.start_link()
    {:ok, watcher} = WM.Containers.Relay.start_link children: [child], report_to: self()
    assert_received {:RC_initialized, ^watcher}

    assert WM.Container.get_state(watcher, :c_children) == [child]
    assert WM.Container.get_state(child, :c_parent) == watcher
    child_index = 0

    WM.Container.hint_focus(child)
    :timer.sleep(100)

    assert_received {:RC_got_focus_order, ^watcher, ^child_index}

    WM.Container.stop child
    WM.Container.stop watcher
  end

  # TODO: reordering of children
end

