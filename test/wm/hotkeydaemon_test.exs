defmodule WM.HotKeyDaemonTest do
  use ExUnit.Case

  # Note that this test suite requires that an X server without window
  # manager be running (Xephyr does the trick) on $DISPLAY

  @timeout_after 100 # Miliseconds

  defp mk_chord(mods, key), do: %WM.HotKeyDaemon.Chord{modifiers: MapSet.new(mods), key: key}
  defp mk_seq(chords), do: %WM.HotKeyDaemon.Sequence{chords: chords}

  defp snd_hk(svr, %WM.HotKeyDaemon.Sequence{chords: chords}) do
    [%XEB.Proto.Core.SCREEN{root: root} | _] = XEB.Server.get_screens()
    Enum.map chords, fn %WM.HotKeyDaemon.Chord{key: k, modifiers: mods} ->
      send svr, {:X_event, root, %XEB.Proto.Core.KeyPressEvent{
        detail: k, time: 0, root: root, event: root, child: :None, same_screen: true,
        root_y: 0, root_x: 0, event_x: 0, event_y: 0, state: mods,}}
      :timer.sleep 10
    end
  end

  setup_all do
    hotkeys = %{}
    {:ok, svr} = WM.HotKeyDaemon.start_link(nil, hotkeys)
    [screen | _] = XEB.Server.get_screens()

    on_exit fn ->
      WM.HotKeyDaemon.terminate(svr)
    end

    {:ok, %{svr: svr, root: screen.root}}
  end

  test "reacts to simple keystrokes", state do
    curr = self()
    run = fn -> send curr, :simple_keystroke end
    hk = mk_seq [mk_chord([:Control], :a)]

    WM.HotKeyDaemon.add_hotkeys(state.svr, %{hk => run})

    snd_hk state.svr, hk
    res = receive do
      :simple_keystroke -> :simple_keystroke
    after
      @timeout_after -> :timed_out
    end

    assert res == :simple_keystroke
    WM.HotKeyDaemon.del_hotkeys(state.svr, [hk])
  end

  test "reacts to keystroke sequences", state do
    curr = self()
    run = fn -> send curr, :got_sequence end
    seq = mk_seq [mk_chord([:Control], :t),
                  mk_chord([:Control], :g)]

    WM.HotKeyDaemon.add_hotkeys(state.svr, %{seq => run})

    snd_hk state.svr, seq
    res = receive do
      :got_sequence -> :got_sequence
    after
      @timeout_after -> :timed_out
    end

    assert res == :got_sequence
    WM.HotKeyDaemon.del_hotkeys(state.svr, [seq])
  end

  test "doesn't react to incomplete sequences that are broken", state do
    curr = self()
    run = fn -> send curr, :got_sequence end
    seq = mk_seq [mk_chord([:Control], :e),
                  mk_chord([:Control], :c),
                  mk_chord([:Control], :f)]

    WM.HotKeyDaemon.add_hotkeys(state.svr, %{seq => run})

    snd_hk state.svr, mk_seq [mk_chord([:Control], :e),
                              mk_chord([:Control], :c),
                              mk_chord([:Control], :u)]
    res = receive do
      :got_sequence -> :got_sequence
    after
      @timeout_after -> :timed_out
    end

    assert res == :timed_out
    WM.HotKeyDaemon.del_hotkeys(state.svr, [seq])
  end

  test "reacts to a simple keystroke that breaks a known sequence", state do
    curr = self()

    # Sequence definition
    s_run = fn -> send curr, :got_sequence end
    seq = mk_seq [mk_chord([:Control], :w),
                  mk_chord([:Control], :r),
                  mk_chord([:Control], :m)]

    # Keystroke definition
    k_run = fn -> send curr, :got_keystroke end
    stroke = mk_seq [mk_chord([:Control], :n)]

    WM.HotKeyDaemon.add_hotkeys(state.svr, %{seq => s_run, stroke => k_run})

    snd_hk state.svr, mk_seq [mk_chord([:Control], :w),
                              mk_chord([:Control], :r),
                              mk_chord([:Control], :n)]
    res = receive do
      :got_sequence -> :got_sequence
      :got_keystroke -> :got_keystroke
    after
      @timeout_after -> :timed_out
    end

    assert res == :got_keystroke
    WM.HotKeyDaemon.del_hotkeys(state.svr, [seq, stroke])
  end

  test "ignores CapsLock", state do
    curr = self()
    run = fn -> send curr, :simple_keystroke end
    hk = mk_seq [mk_chord([:Control, :Lock], :a)]

    WM.HotKeyDaemon.add_hotkeys(state.svr, %{hk => run})

    snd_hk state.svr, hk
    res = receive do
      :simple_keystroke -> :simple_keystroke
    after
      @timeout_after -> :timed_out
    end

    assert res == :simple_keystroke
    WM.HotKeyDaemon.del_hotkeys(state.svr, [hk])
  end
end

