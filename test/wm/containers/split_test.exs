defmodule WM.Containers.SplitTest do
  use ExUnit.Case

  @base_geom %WM.Container.Geometry{
    x: 0,        y: 0,
    width: 1000, height: 1000,
    mapped: true
  }

  test "Takes proportions into account when adding children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 250
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 500
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 750
    }

    WM.Container.stop split
  end

  test "Takes proportions into account when removing end children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    {:ok, ch5} = WM.Containers.Blank.start parent: split
    WM.Container.stop ch5

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 250
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 500
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 750
    }

    WM.Container.stop split
  end

  test "Takes proportions into account when removing middle children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..5 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.fetch!(2) |> elem(1) |> WM.Container.stop
    chn = List.delete_at chn, 2

    :timer.sleep(100)

    [geom1, geom3, geom4, geom5] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 0
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 250
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 500
    }

    assert geom5 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 750
    }

    WM.Container.stop split
  end

  test "Takes gaps into account when adding children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 10

    chn = 1..3 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    [geom1, geom2, geom3] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 327, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 327, x: 337
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 326, x: 674
    }

    WM.Container.stop split
  end

  test "Takes gaps into account when removing end children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 10

    chn = 1..3 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    {:ok, ch4} = WM.Containers.Blank.start parent: split
    WM.Container.stop ch4

    :timer.sleep(100)

    [geom1, geom2, geom3] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 327, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 327, x: 337
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 326, x: 674
    }

    WM.Container.stop split
  end

  test "Takes gaps into account when removing middle children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 10

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.at(2) |> elem(1) |> WM.Container.stop
    chn = List.delete_at chn, 2

    :timer.sleep(100)

    [geom1, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 327, x: 0
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 327, x: 337
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 326, x: 674
    }

    WM.Container.stop split
  end

  @scaled_geom %WM.Container.Geometry{
    x: 0,         y: 0,
    height: 1000, width: 2000,
    mapped: true
  }

  test "Preserves proportions when being resized" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    WM.Container.set_geometry split, @scaled_geom

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @scaled_geom | width: 500, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @scaled_geom | width: 500, x: 500
    }

    assert geom3 == %WM.Container.Geometry{
      @scaled_geom | width: 500, x: 1000
    }

    assert geom4 == %WM.Container.Geometry{
      @scaled_geom | width: 500, x: 1500
    }

    WM.Container.stop split
  end

  test "Takes gaps into account when being resized" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 10

    chn = 1..3 |> Enum.map( fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    WM.Container.set_geometry split, @scaled_geom

    :timer.sleep(100)

    [geom1, geom2, geom3] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @scaled_geom | width: 660, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @scaled_geom | width: 660, x: 670
    }

    assert geom3 == %WM.Container.Geometry{
      @scaled_geom | width: 660, x: 1340
    }

    WM.Container.stop split
  end

  @shifted_geom %WM.Container.Geometry{
    x: 100,      y: 100,
    width: 1000, height: 1000,
    mapped: true
  }

  test "Moves children when moved" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    WM.Container.set_geometry split, @shifted_geom

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @shifted_geom | width: 250, x: 100
    }

    assert geom2 == %WM.Container.Geometry{
      @shifted_geom | width: 250, x: 350
    }

    assert geom3 == %WM.Container.Geometry{
      @shifted_geom | width: 250, x: 600
    }

    assert geom4 == %WM.Container.Geometry{
      @shifted_geom | width: 250, x: 850
    }

    WM.Container.stop split
  end

  test "Applies perpendicular-size geometry orders on middle children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.at(1) |> elem(1) |> WM.Container.hint_size({:mod, 30}, nil)

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 240, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 280, x: 240
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 240, x: 520
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 240, x: 760
    }

    WM.Container.stop split
  end

  test "Applies perpendicular-size geometry orders on end children" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.at(0) |> elem(1) |> WM.Container.hint_size({:mod, 30}, nil)

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 280, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 240, x: 280
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 240, x: 520
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 240, x: 760
    }

    WM.Container.stop split
  end

  test "Limits perpendicular-size geometry orders that don't fit the geometry" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.at(0) |> elem(1) |> WM.Container.hint_size(9000, nil)

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 970, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 10, x: 970
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 10, x: 980
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 10, x: 990
    }

    WM.Container.stop split
  end

  test "Ignores position orders" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.at(0) |> elem(1) |> WM.Container.hint_position(34, 61)

    :timer.sleep(100)

    [geom1, geom2, geom3, geom4] = Enum.map chn, fn {:ok, ch} ->
      WM.Container.get_state ch, :c_geometry
    end

    assert geom1 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 0
    }

    assert geom2 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 250
    }

    assert geom3 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 500
    }

    assert geom4 == %WM.Container.Geometry{
      @base_geom | width: 250, x: 750
    }

    WM.Container.stop split
  end

  test "Propagates parallel-size geometry orders to parent" do
    {:ok, split} = WM.Containers.Split.start direction: :vertical,
                                             geometry: @base_geom,
                                                  gap: 0

    {:ok, watcher} = WM.Containers.Relay.start children: [split],
                                              report_to: self()
    assert_received {:RC_initialized, ^watcher}

    chn = 1..4 |> Enum.map(fn _ ->
      WM.Containers.Blank.start parent: split
    end) |> Enum.reverse

    :timer.sleep(100)

    chn |> Enum.at(0) |> elem(1) |> WM.Container.hint_size(nil, 200)
    :timer.sleep(100)
    assert_received {:RC_got_geometry_order, ^watcher, 
                     {0, %WM.Container.Geometry{height: 200}}}

    WM.Container.stop split
  end
end

