# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.Containers.TopLevel do
  @moduledoc ~S"""
  A top-level container is a root of the container tree. Toplevel containers do not have parents and are instead directly managed by WM.Manager. The main use of toplevel containers is to be a universal endall to orders that go up the container tree by either taking some kind of action or simply not transmit anything at all.
  A top-level container can only have one child.
  """

  use WM.Container

  #############################################################################
  # Public API

  @doc ~S"""
  Starts a top-level container process with link.
  """
  def start_link(options \\ []) do
    WM.Container.start_link __MODULE__, options
  end

  @doc ~S"""
  Starts a top-level container process without a link. Options are the same as for `start_link`.
  """
  def start(options \\ []) do
    WM.Container.start __MODULE__, options
  end

  #############################################################################
  # WM.Container callbacks

  # Only child policy...
  def handle_add_child(ch, %{c_children: [], c_geometry: geom} = state) do
    WM.Container.set_geometry(ch, geom)
    {:ok, state}
  end

  def handle_add_child(_, state) do
    {:deny, state}
  end

  # Handle all orders so they don't keep on going up the container tree

  def handle_geometry_order(_, _, state) do
    # Geometry orders are ignored ; the only child of the top-level container
    # must always have the same geometry as the top-level container (although
    # the top-level container itself may be resized).
    {:ok, state}
  end

  def handle_focus_order(from, state) do
    # TODO: what to do in this situation ?
    {:ok, state}
  end

  def handle_move_focus(_, state) do
    # Focus movements are ignored, of course, since the container only has one child
    {:ok, state}
  end
end


