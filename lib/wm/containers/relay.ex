# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.Containers.Relay do
  @moduledoc ~S"""
  This module defines a container that behaves like a regular container, but relays every activity to a specified process.
  """
  use WM.Container

  # HACK: The default implementation of callbacks is literally copied in place,
  # which constitutes a very bad code duplication offense. If something unexpected
  # happens, check if the code here is up to date with the default implementation
  # of Container callbacks.

  @specific_state %{
    # The process to relay activity to
    m_report_to: nil
  }

  @doc ~S"""
  This is the same as the normal 'start' function for containers, with one more possible option: the option name is `:report_to` and its value should be the process to report activity to. If it is `nil`, the container will behave exactly like `WM.Containers.Blank`.
  """
  def start_link(options \\ []) do
    WM.Container.start_link __MODULE__, options
  end

  def start(options \\ []) do
    WM.Container.start __MODULE__, options
  end

  def init(options, state) do
    new_state = Map.merge state, %{@specific_state | m_report_to: options[:report_to]}
    report new_state, :RC_initialized
  end

  def terminate(reason, state) do
    report state, :RC_terminating, reason
    reason
  end

  def handle_info(message, state),
    do: report state, :RC_got_info, message

  def handle_add_child(child, state),
    do: report state, :RC_adding_child, child

  def handle_rem_child(child, state),
    do: report state, :RC_removing_child, child

  def handle_parent_change(new_parent, state),
    do: report state, :RC_changing_parent, new_parent

  def handle_set_geometry(new_geometry, %{c_children: children} = state) do
    Enum.map children, &WM.Container.set_geometry(&1, new_geometry)
    state = %{state | c_geometry: new_geometry}
    report state, :RC_changing_geometry, new_geometry
  end

  def handle_geometry_hint(new_geometry, %{c_parent: parent} = state) do
    WM.Container.order_geometry parent, new_geometry
    report state, :RC_got_geometry_hint, new_geometry
  end

  def handle_geometry_order(from, new_geometry, %{c_parent: parent} = state) do
    WM.Container.order_geometry parent, new_geometry
    report state, :RC_got_geometry_order, {from, new_geometry}
  end

  def handle_set_focus(%{c_children: children, c_focused_child: index} = state) do
    WM.Container.set_focus Enum.at(children, index)
    report state, :RC_setting_focus
  end

  def handle_focus_hint(%{c_parent: parent} = state) do
    WM.Container.order_focus parent
    report state, :RC_got_focus_hint
  end

  def handle_focus_order(from, %{c_parent: parent} = state) do
    WM.Container.order_focus parent
    state = %{state | c_focused_child: from}
    report state, :RC_got_focus_order, from
  end

  def handle_move_focus(n, %{c_children: children} = state)
                       when is_integer(n) and n < length(children) do
    WM.Container.set_focus Enum.at(children, n)
    state = %{state | c_focused_child: n}
    report state, :RC_got_move_focus, n
  end

  def handle_move_focus(:previous, %{c_children: children, c_focused_child: index} = state) do
    new_index = rem(index - 1, length(children))
    WM.Container.set_focus Enum.at(children, new_index)
    state = %{state | c_focused_child: new_index}
    report state, :RC_got_move_focus, :previous
  end

  def handle_move_focus(:next, %{c_children: children, c_focused_child: index} = state) do
    new_index = rem(index + 1, length(children))
    WM.Container.set_focus Enum.at(children, new_index)
    state = %{state | c_focused_child: new_index}
    report state, :RC_got_move_focus, :next
  end

  def handle_move_focus(move, %{c_parent: parent} = state) do
    WM.Container.move_focus parent, move
    report state, :RC_got_move_focus, move
  end

  # For convenience
  defp report(state, keyword, payload \\ nil)
  defp report(%{m_report_to: pid} = state, keyword, nil) when is_pid(pid) do
    send pid, {keyword, self()}
    {:ok, state}
  end

  defp report(%{m_report_to: pid} = state, keyword, payload) when is_pid(pid) do
    send pid, {keyword, self(), payload}
    {:ok, state}
  end

  defp report(state, _, _) do
    {:ok, state}
  end
end


