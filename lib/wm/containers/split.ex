# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.Containers.Split do
  @moduledoc ~S"""
  A Split Container is a container that splits the space among its children, either vertically or horizontally, and possibly with a gap between them.
  """

  require Logger
  use WM.Container

  @specific_variables %{
    # The direction of the split. May be `:horizontal` (the direction of
    # the width) or `:vertical` (direction of the height).
    # In order to abstract away the direction, the directions "parallel"
    # and "perpendicular" are used throughout this module.
    m_direction: :vertical,

    # The gap between each pair of adjacent children.
    m_gap: 0,

    # The size of each child in pixels
    m_sizes: []
  }

  #############################################################################
  # Public API

  # Starting options and their values
  @start_options %{
    direction: :vertical,
    gap: 0
  }

  @doc ~S"""
  Starts a split container process with link.
  Possible options are all options of the WM.Container behaviour, plus these:

  * `direction:`: The direction of the split, either `:vertical` (by default) or `:horizontal`.
  * `gap:`: The size in pixels of the gap between adjacent children. 0 by default.
  """
  def start_link(options \\ []) do
    WM.Container.start_link __MODULE__, options
  end

  @doc ~S"""
  Starts a split container process without a link. Options are the same as for `start_link`.
  """
  def start(options \\ []) do
    WM.Container.start __MODULE__, options
  end

  #############################################################################
  # WM.Container callbacks

  def init(args, state) do
    opts = Map.merge @start_options, args
    st = Map.merge state, %{@specific_variables |
      m_direction: opts.direction,
      m_gap: opts.gap
    }

    {:ok, st}
  end

  def handle_add_child(child, %{m_gap: gap, c_geometry: geom, c_children: chn, m_sizes: sizes} = state) do
    child_size = if chn == [] do
      perpendicular_size(geom, state)
    else
      self_size = perpendicular_size(geom, state) - gap * (length(chn) - 1)
      div(self_size, length(chn))
    end

    conform_children %{state |
      c_children: [child | chn],
      m_sizes: [child_size | sizes]
    }
  end

  def handle_rem_child(0, %{c_children: [ch]} = state) do
    WM.Container.set_mapping_status ch, false
    {:ok, %{state | c_children: [], m_sizes: []}}
  end

  def handle_rem_child(index, %{c_children: chn, m_sizes: sizes} = state) do
    conform_children %{state |
      c_children: List.delete_at(chn, index),
      m_sizes: List.delete_at(sizes, index)
    }
  end

  def handle_set_geometry(new_geom, state),
    do: conform_children %{state | c_geometry: new_geom}

  def handle_geometry_order(sender, order, %{m_sizes: sizes} = state) do
    new_state = if length(sizes) > 1 do
      perp_diff = case perpendicular_size(order, state) do
        nil -> 0
        {:mod, x} -> x
        x when is_integer(x) -> x - Enum.at(sizes, sender)
      end
      adjust = div(perp_diff, length(sizes) - 1)

      new_sizes = sizes
      |> List.update_at(sender, &(&1 + perp_diff))
      |> map_skip(sender, &(&1 - adjust))

      %{state | m_sizes: new_sizes}
    else
      state
    end

    parallel_order = parallel_size(order, state)
    if parallel_order != nil do
      if state.m_direction == :vertical do
        WM.Container.order_size state.c_parent, nil, parallel_order
      else
        WM.Container.order_size state.c_parent, parallel_order, nil
      end
    end

    # new_state = case order.mapped do  # TODO
    #   :toggle ->
    #   :nil ->
    #   bool when is_boolean(bool) ->
    # end
    conform_children new_state
  end

  def handle_move_focus(:up, %{m_direction: :vertical} = state),
    do: WM.Containers.Blank.handle_move_focus(:previous, state)
  def handle_move_focus(:down, %{m_direction: :vertical} = state),
    do: WM.Containers.Blank.handle_move_focus(:next, state)

  def handle_move_focus(:left, %{m_direction: :horizontal} = state),
    do: WM.Containers.Blank.handle_move_focus(:previous, state)
  def handle_move_focus(:right, %{m_direction: :horizontal} = state),
    do: WM.Containers.Blank.handle_move_focus(:next, state)

  def handle_move_focus(m, s), do: WM.Containers.Blank.handle_move_focus(m, s)

  #############################################################################
  # Internals

  # Conform the actual layout of the children to the given state.
  # Returns {:ok, new_state} or {:stop, reason} ; meant to be used as a tail call.
  # If in the given state, the sizes in the 'm_size' state variable don't add
  # up to the size of the container (the c_geometry variable), then the sizes
  # of the children will be adjusted by cross-multiplication. The gaps are
  # also added.
  defp conform_children(%{c_children: []} = state), do: {:ok, state}
  defp conform_children(state) do
    new_state = adjust_sizes state
    %{m_direction: dir,
      m_sizes: sizes,
      m_gap: gap,
      c_children: chn,
      c_geometry: geom} = new_state

    if dir == :vertical do
      Enum.zip(chn, sizes)
      |> Enum.reduce(geom.x, fn ({ch, sz}, pos) ->
        WM.Container.set_geometry ch, %WM.Container.Geometry{
          geom | width: sz, x: pos
        }
        pos + sz + gap
      end)
    else
      Enum.zip(chn, sizes)
      |> Enum.reduce(geom.y, fn ({ch, sz}, pos) ->
        WM.Container.set_geometry ch, %WM.Container.Geometry{
          geom | height: sz, y: pos
        }
        pos + sz + gap
      end)
    end

    {:ok, new_state}
  end

  # Adjust the m_sizes state variable to match the geometry of the container
  # by cross-multiplication. Returns the new state. Takes the gap into account.
  defp adjust_sizes(%{c_geometry: geom, m_sizes: sizes, m_gap: gap} = state) do
    self_size = perpendicular_size(geom, state) - gap * (length(sizes) - 1)
    grouped_size = Enum.sum sizes
    min_ch_size = minimum_size state

    new_sizes = if self_size == grouped_size do
      sizes
    else
      scale = self_size / grouped_size
      scaled_sizes = Enum.map sizes, fn x -> (x * scale) |> Float.floor |> trunc end
      margin = self_size - Enum.sum(scaled_sizes)
      add_to_list scaled_sizes, margin
    end

    new_sizes = if Enum.any?(new_sizes, &(&1 < min_ch_size)) do
      diff = new_sizes
      |> Enum.filter(&(&1 < min_ch_size))
      |> Enum.map(&(min_ch_size - &1))
      |> Enum.sum

      new_sizes
      |> Enum.map(fn x -> if x < min_ch_size do min_ch_size else x end end)
      |> sub_from_list(diff)
    else
      new_sizes
    end

    %{state | m_sizes: new_sizes}
  end

  # Add a number to a list of integers by spreading it over the list
  defp add_to_list(_, x) when x < 0, do: raise "Cannot add negative int to list: #{inspect x}"
  defp add_to_list(ls, 0), do: ls
  defp add_to_list(ls, x) do
    # TODO: find a better way to do that
    min = Enum.min ls
    min_index = Enum.find_index ls, &(&1 == min)
    final_list = List.update_at ls, min_index, &(&1 + 1)
    add_to_list final_list, x - 1
  end

  # Opposite of add_to_list: removes a number from a list of integers by
  # decrementing the largest element on each pass.
  defp sub_from_list(_, x) when x < 0, do: raise "Cannot subtract negative int from list #{inspect x}"
  defp sub_from_list(ls, 0), do: ls
  defp sub_from_list(ls, x) do
    # TODO: find a better way to do that
    max = Enum.max ls
    max_index = Enum.find_index ls, &(&1 == max)
    final_list = List.update_at ls, max_index, &(&1 - 1)
    sub_from_list final_list, x - 1
  end

  # Calculate the minimum size in pixels for a child to have. The result of
  # the function does NOT depend on the 'm_sizes' state variable or the
  # geometries of the children.
  defp minimum_size(%{c_children: chn, c_geometry: geom} = state) do
    # The 25 'magic number' is maybe a bit too high... FIXME
    trunc(perpendicular_size(geom, state) / (length(chn) * 25))
  end

  # Map a function over a list except the given index
  defp map_skip(ls, skip, f, i \\ 0)
  defp map_skip([], _, _, _),
    do: []
  defp map_skip([x | xs], skip, f, skip),
    do: [x | map_skip(xs, skip, f, skip + 1)]
  defp map_skip([x | xs], skip, f, i),
    do: [f.(x) | map_skip(xs, skip, f, i + 1)]


  defp parallel_size(%_{width: w}, %{m_direction: :horizontal}), do: w
  defp parallel_size(%_{height: h}, %{m_direction: :vertical}), do: h

  defp perpendicular_size(%_{width: w}, %{m_direction: :vertical}), do: w
  defp perpendicular_size(%_{height: h}, %{m_direction: :horizontal}), do: h
end

