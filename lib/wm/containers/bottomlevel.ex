# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.Containers.BottomLevel do
  @moduledoc ~S"""
  A Bottom-level container is a leaf of the container tree. Each bottom-level container manages zero or one X window(s) by sending the X server requests to make its own geometry match the window's.
  """

  require Logger
  use WM.Container

  # All the state variables that are specific to bottom-level containers.
  @specific_variables %{
    # The window
    m_win: nil
  }

  #############################################################################
  # Public API

  # Starting options and their values
  @start_options %{
    window: nil
  }

  @doc ~S"""
  Starts a bottom-level container process with link.
  Possible options are all options of the WM.Container behaviour (except the `:children` option, which is ignored), plus these:

  * `window:`: an XEB.Proto.Core.WINDOW struct, taken as the window to manage. `nil` by default.
  """
  def start_link(options \\ []) do
    WM.Container.start_link __MODULE__, options
  end

  @doc ~S"""
  Starts a bottom-level container process without a link. Options are the same as for `start_link`.
  """
  def start(options \\ []) do
    WM.Container.start __MODULE__, options
  end

  @doc ~S"""
  Causes the specified container to adopt the manage the given window. The window will be configured to fit the current geometry of the container. The `window` argument may be `nil`, in which case the wontainer will simply stop managing its current window without adopting a new one.
  """
  def adopt_window(cont, win) do
    pid = if is_atom(cont) do Process.whereis cont else cont end
    send pid, {:adopt_window, win}
  end

  #############################################################################
  # WM.Container callbacks

  def init(args, state) do
    opts = Map.merge @start_options, args
    st = Map.merge state, @specific_variables

    handle_info({:adopt_window, opts.window}, st)
  end

  def handle_add_child(_, state) do
    # CHILDFREE FOR LIFE!
    {:deny, state}
  end

  def handle_set_geometry(geom, state) do
    new_state = conform_window %{state | c_geometry: geom}
    {:ok, new_state}
  end

  def handle_set_focus(%{m_win: win} = state) do
    XEB.Core.set_input_focus revert_to: :None, focus: win, time: :CurrentTime
    {:ok, state}
  end

  def handle_info({:adopt_window, win}, %{m_win: former_win} = state) do
    if former_win, do: XEB.ResID.id_release former_win.xid
    if win, do: XEB.ResID.id_seize win.xid
    new_state = conform_window %{state | m_win: win}
    {:ok, new_state}
  end

  def handle_info({:X_event, win, %XEB.Proto.Core.DestroyNotifyEvent{window: win}},
                  %{m_win: win}) do
    WM.Manager.destroy_notify win
    {:stop, :normal}
  end

  def handle_info(info, state) do
    Logger.debug "Bottom level container #{inspect self()} received message: #{inspect info}. State is #{inspect state}"
    {:ok, state}
  end

  def terminate(_reason, %{m_win: win}) do
    XEB.ResID.id_release win.xid
    :ok
  end

  ############################################################################
  # Internals

  # Configure the window from the state to conform the container geometry.
  # Returns the new state.
  defp conform_window(%{m_win: nil} = state), do: state
  defp conform_window(%{m_win: win, c_geometry: geom} = state) do
    if geom.width == 0 or geom.height == 0,
      do: raise "Window dimensions must be non-zero"

    XEB.Core.configure_window window: win,
                              x: geom.x, y: geom.y,
                              width: geom.width, height: geom.height

    if geom.mapped do
      XEB.Core.map_window window: win
    else
      XEB.Core.unmap_window window: win
    end

    state
  end
end

