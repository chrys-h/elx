# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.Manager do
  @moduledoc ~S"""
  Foundations for window management functions. The module creates a process that seizes ownership of the screen's root window's XID (see XEB.ResID) and sets up substructure redirection.
  """

  use GenStateMachine
  require Logger

  # Manager state variables
  @init_state %{
    # Root window.
    root: nil,

    # Map of known managed windows matched to their bottom-level container
    managed: %{},

    # Top level containers PIDs (ie. the containers that are not contained)
    top_conts: [],

    # Default container to add children to (doesn't need to be top-level)
    default_cont: nil
  }

  ###################################################################
  # Public API

  @doc ~S"""
  Start the process and attempt to seize the root window.
  """
  def start_link(name \\ __MODULE__) do
    GenStateMachine.start_link __MODULE__, %{}, name: name
  end

  @doc ~S"""
  Used by bottom-level containers to signal that a window was destroyed.
  """         # TODO: Find more elegant solution...
  def destroy_notify(manager \\ __MODULE__, window) do
    GenStateMachine.cast manager, {:window_destroyed, window}
  end

  ###################################################################
  # Backend functions

  def init(_) do
    # Get info. Like everywhere else, we assume there is only one screen.
    [%XEB.Proto.Core.SCREEN{
      root: root,
      height_in_pixels: scr_h,
      width_in_pixels: scr_w
    } | _] = XEB.Server.get_screens()

    # Seize root window ; We implicitly assume there is only one screen.
    XEB.ResID.id_seize root.xid
    XEB.Core.change_window_attributes window: root, event_mask: [:SubstructureRedirect]

    # Verify root window was succesfully seized
    rxid = root.xid
    state = %{@init_state | root: root}
    receive do
      {:X_error, %XEB.Proto.Core.AccessError{bad_value: ^rxid, major_opcode: 2, minor_opcode: 0}} ->
        # This error is received when a substructure redirection on the root
        # window is attempted but another X11 client already has it.
        raise "Substructure redirection failure. Is another window manager already running?"
    after
      500 -> nil
    end

    # Take control of the already existing substructure
    {:ok, top_cont} = WM.Containers.TopLevel.start name: :toplevel_cont,
      geometry: %WM.Container.Geometry{x: 0, y: 0, width: scr_w, height: scr_h, mapped: true}
    {:ok, split_cont} = WM.Containers.Split.start name: :split_cont,
      direction: :vertical, gap: 10, parent: top_cont

    state = manage_existing_substructure root, %{state | default_cont: split_cont, top_conts: [top_cont]}

    {:ok, :none, state}
  end

  def terminate(reason, _, %{top_conts: conts} = _state) do
    Enum.map conts, &WM.Container.stop(&1, reason)
    Application.stop(:elX)
  end

  def handle_event(:cast, {:window_destroyed, win}, _, state) do
    Logger.debug "WM.Manager notified of destruction of #{inspect win}"
    {:keep_state, handle_destroy_notify(win, state)}
  end

  def handle_event(:info, {:X_error, err}, _, _) do
    Logger.warn "WM.Manager received error: #{inspect err}"
    :keep_state_and_data
  end

  def handle_event(:info, {:X_event, root, %XEB.Proto.Core.MapRequestEvent{window: win}},
                   _, %{root: root} = state) do
    Logger.debug "WM.Manager received Map Request for window #{inspect win}"
    {:keep_state, handle_map_request(win, state)}
  end

  def handle_event(:info, {:X_event, _res, evt}, _, _) do
    Logger.debug "WM.Manager received event: #{inspect evt}"
    :keep_state_and_data
  end

  defp handle_map_request(window, %{managed: managed} = state) do
    Logger.debug "Handling map request for window #{inspect window}"
    if Map.has_key?(managed, window) do
      # If the window is already managed, the corresponding bottom-level container will
      # have received the MapRequest too. We let it handle the situation.
      state
    else
      Logger.debug "    ...Unknown window. Managing..."
      manage_window(window, state)
    end
  end

  defp handle_destroy_notify(win, %{managed: managed} = state) do
    %{state | managed: Map.delete(managed, win)}
  end

  defp manage_existing_substructure(root, state) do
    tree = XEB.Core.query_tree window: root
    Enum.reduce tree.children, state, &manage_window(&1, &2)
  end

  defp manage_window(_, %{default_cont: nil} = state) do
    Logger.warn "Attempting to manage window but no default container set. Ignoring."
    state
  end

  defp manage_window(win, %{default_cont: cont, managed: managed} = state) do
    Logger.debug "Managing window #{inspect win}"
    XEB.Core.change_window_attributes window: win, event_mask: [:StructureNotify]
    {:ok, new_container} = WM.Containers.BottomLevel.start window: win, parent: cont
    Logger.debug "     ...Assigning container #{inspect new_container}"
    %{state | managed: Map.put(managed, win, new_container)}
  end
end

