# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.HotKeyDaemon do
  @moduledoc ~S"""
  Defines a process that receives keyboard events from the X11 server and executes a specific function on each hotkey.
  """

  require Logger

  # A chord is the same concept as SXHKD internally uses: a key and a
  # set of modifiers being down at the same time. The key is represented
  # as a KEYCODE, a KEYSYM, or an atom that names a KEYSYM, and the
  # modifiers are a MapSet of atoms that are values of ModMask, KEYCODEs
  # or KEYSYMs of modifier keys (as recognized by XEB.Mapping) or atoms
  # that name suck KEYSYMs (as recognized by XEB.Keysyms)
  defmodule Chord do
    defstruct [:key, :modifiers]
  end

  # A sequence is a succession of one or more chords.
  # Note that this struct is merely used for type safety when interfacing
  # with other components ; sequences are internally represented as bare
  # lists of chords.
  defmodule Sequence do
    defstruct [:chords]
  end

  # The modifier keys that are not taken into account for hotkeys.
  # Note that these are keysyms, not the actual KeyButMask-s or ModMask-s
  @ignore_modifiers [:Num_Lock, :Caps_Lock, :Scroll_lock]

  #############################################################################
  # Public API

  @doc ~S"""
  Start the hotkey daemon with the specified hotkeys. The expected format for the hotkeys is a map that maps MapSets of keysyms to functions. The function is then run when the corresponding hotkey is received.
  """
  def start_link(name \\ __MODULE__, hotkeys \\ %{}) do
    pid = spawn_link fn -> init(hotkeys) end
    if name do Process.register pid, name end
    {:ok, pid}
  end

  @doc ~S"""
  Add hotkeys to the daemon. If a hotkey is already recognized, it will be updated.
  """
  def add_hotkeys(hkd \\ __MODULE__, hotkeys) do
    pid = if is_atom(hkd) do Process.whereis hkd else hkd end
    send pid, {:add_hotkeys, hotkeys}
  end

  @doc ~S"""
  Remove hotkeys from the daemon. If a hotkey is not already recognized, it will be ignored.
  """
  def del_hotkeys(hkd \\ __MODULE__, hotkeys) do
    pid = if is_atom(hkd) do Process.whereis hkd else hkd end
    send pid, {:del_hotkeys, hotkeys}
  end

  @doc ~S"""
  Terminate the daemon.
  """
  def terminate(hkd \\ __MODULE__) do
    pid = if is_atom(hkd) do Process.whereis hkd else hkd end
    send pid, :sigterm
  end

  #############################################################################
  # Backend functions

  defp init(hotkeys) do
    Logger.debug "WM.HotKeyDaemon.init"
    [%XEB.Proto.Core.SCREEN{root: root} | _] = XEB.Server.get_screens()
    XEB.ResID.id_seize root.xid
    state = %{hotkeys: %{}, root: root, awaiting: %{}}
    state = register_hks hotkeys, state
    loop state
  end

  defp loop(%{root: root} = state) do
    Logger.debug "WM.HotKeyDaemon.loop"
    receive do
      {:X_event, ^root, %XEB.Proto.Core.KeyPressEvent{state: mods, detail: key}} ->
        Logger.debug "HKD: received event"
        new_state = handle_chord(key, mods, state)
        loop new_state

      {:X_error, %XEB.Proto.Core.AccessError{major_opcode: 33, minor_opcode: 0, bad_value: v}} ->
        # This error is received when attempting a GrabKey on a key that
        # is already grabbed by another client.
        Logger.warn "Could not grab key: access error, bad value: #{inspect v}"
        loop state

      {:add_hotkeys, hks} ->
        new_state = register_hks hks, state
        loop new_state

      {:del_hotkeys, hks} ->
        new_state = unregister_hks hks, state
        loop new_state

      :sigterm -> teardown state
      x ->
        Logger.debug "WM.HotKeyDaemon.loop: got #{inspect x} ; Ignoring."
        loop state
    end
  end

  defp teardown(%{hotkeys: hks, root: root} = state) do
    Logger.debug "WM.HotKeyDaemon.teardown"
    XEB.ResID.id_release root.xid
    unregister_hks hks, state
  end

  # Reset the 'pristine' awaiting tree from the list of hotkeys and the grabs.
  # Note that the tree is reconstructed from the list everytime, which
  # may cause performance issues if the list is really long.
  defp reset_awaiting(%{awaiting: a, hotkeys: hks} = state) do
    Logger.debug "WM.HotKeyDaemon.reset_awaiting: hotkeys = #{inspect hks}"
    new_tree = Enum.reduce hks, %{}, fn ({seq, fun}, acc) ->
      incorporate_seq(acc, seq, fun)
    end

    currently_grabbed = a |> Map.keys |> MapSet.new
    tree_grabs = new_tree |> Map.keys |> MapSet.new
    for ch <- MapSet.difference(currently_grabbed, tree_grabs), do: ungrab_chord ch, state
    for ch <- MapSet.difference(tree_grabs, currently_grabbed), do: grab_chord ch, state

    %{state | awaiting: new_tree}
  end

  defp incorporate_seq(map, [last | []], fun), do: Map.put_new(map, last, {:run_fun, fun})
  defp incorporate_seq(map, [seq_hd | seq_tl], fun) do
    Logger.debug "WM.HotKeyDaemon.incorporate_seq"
    # TODO: Maybe Map.update could be used here?
    val = case Map.get(map, seq_hd, {:await, %{}}) do
      {:run_fun, _} = final -> final
      {:await, inner_map} -> {:await, incorporate_seq(inner_map, seq_tl, fun)}
    end
    Map.put map, seq_hd, val
  end

  #############################################################################
  # Specific event handling

  defp handle_chord(k, m, s),
    do: handle_chord(%WM.HotKeyDaemon.Chord{key: k, modifiers: m}, s)

  defp handle_chord(chord, %{awaiting: map} = state) do
    Logger.debug "WM.HotKeyDaemon.handle_chord ; awaiting: #{inspect map}, got chord: #{inspect chord}"
    %WM.HotKeyDaemon.Chord{key: r_key, modifiers: r_mods} = resolve_chord_KeyButMask chord
    resolved = %WM.HotKeyDaemon.Chord{
      modifiers: MapSet.difference(r_mods, ignore_KeyButMasks()),
      key: r_key
    }

    case Map.fetch(map, resolved) do
      {:ok, {:await, add_map}} ->
        chord |> resolve_chord_KeyButMask |> ungrab_chord(state)
        Enum.map Map.keys(add_map), &grab_chord(&1, state)
        new_map = map |> Map.delete(chord) |> Map.merge(add_map)
        %{state | awaiting: new_map}

      {:ok, {:run_fun, fun}} ->
        fun.()
        # The 'pristine' awaiting state is reset everytime. It can be a costly
        # operation, so calling it everytime may become a performance issue.
        reset_awaiting state

      :error ->
        Logger.warn "WM.HotKeyDaemon got unknown chord. Is there a bug in the HotKeyDaemon grab update mechanism?"
        state

      x ->
        Logger.warn "Something really weird happened: #{inspect x}"
        state
    end
  end

  #############################################################################
  # Hotkey list management

  defp register_hks(hotkeys, state) do
    Logger.debug "WM.HotKeyDaemon.register_hks: #{inspect hotkeys}"
    new_state = Enum.reduce hotkeys, state, fn ({hk, fun}, st) -> register_hk(hk, fun, st, false) end
    reset_awaiting new_state
  end

  defp register_hk(%WM.HotKeyDaemon.Sequence{chords: chords}, fun,
                   %{hotkeys: hks} = state, do_reset) do
    Logger.debug "WM.HotKeyDaemon.register_hk: #{inspect chords}"
    resolved_chords = Enum.map chords, fn ch ->
      %WM.HotKeyDaemon.Chord{modifiers: mods, key: k} = resolve_chord_KeyButMask ch
      %WM.HotKeyDaemon.Chord{key: k, modifiers: MapSet.difference(mods, ignore_KeyButMasks())}
    end

    new_hks = Map.put hks, resolved_chords, fun
    new_state = %{state | hotkeys: new_hks}
    if do_reset do reset_awaiting new_state else new_state end
  end

  defp unregister_hks(hotkeys, state) do
    Logger.debug "WM.HotKeyDaemon.unregister_hks"
    new_state = Enum.reduce hotkeys, state, fn (hk, st) -> unregister_hk(hk, st, false) end
    reset_awaiting new_state
  end

  defp unregister_hk(%WM.HotKeyDaemon.Sequence{chords: chords},
                     %{hotkeys: hks} = state, do_reset) do
    Logger.debug "WM.HotKeyDaemon.unregister_hk"
    resolved_chords = Enum.map chords, fn ch ->
      %WM.HotKeyDaemon.Chord{modifiers: mods, key: k} = resolve_chord_KeyButMask ch
      %WM.HotKeyDaemon.Chord{key: k, modifiers: MapSet.difference(mods, ignore_KeyButMasks())}
    end
    new_hks = Map.delete hks, resolved_chords
    new_state = %{state | hotkeys: new_hks}
    if do_reset do reset_awaiting new_state else new_state end
  end

  # Turn the key into a valid KEYCODE and the modifiers into a valid
  # ModMask specification.
  defp resolve_chord_ModMask(%WM.HotKeyDaemon.Chord{modifiers: mods, key: key}) do
    Logger.debug "WM.HotKeyDaemon.resolve_chord_ModMask: key = #{inspect key}, modifiers = #{inspect mods}"
    new_key = case key do
      %XEB.Proto.Core.KEYCODE{} -> key
      _ -> XEB.Mapping.lookup_keysym key
    end

    new_mods = mods |> Enum.map(fn k ->
      case XEB.Mapping.lookup_mod_reverse(k) do
        nil -> raise "Invalid modifier #{inspect k}"
        res -> res
      end
    end) |> MapSet.new

    %WM.HotKeyDaemon.Chord{modifiers: new_mods, key: new_key}
  end

  # Turn the key into a valid KEYCODE and the modifiers into a valid
  # KeyButMask specification that can be compared to the one found in
  # KeyPressEvent events. HACK: Code duplication
  defp resolve_chord_KeyButMask(%WM.HotKeyDaemon.Chord{} = chord) do
    Logger.debug "WM.HotKeyDaemon.resolve_chord_KeyButMask: #{inspect chord}"
    %WM.HotKeyDaemon.Chord{modifiers: mods, key: key} = resolve_chord_ModMask chord
    %WM.HotKeyDaemon.Chord{modifiers: XEB.Mapping.to_KeyButMask(mods), key: key}
  end

  #############################################################################
  # Keyboard grabs

  defp grab_chord(%WM.HotKeyDaemon.Chord{key: key, modifiers: mods} = chord,
                  %{root: root} = _state) do
    Logger.debug "WM.HotKeyDaemon.grab_chord #{inspect chord}"
    for set <- power_set(ignore_ModMasks()) do
      Logger.debug "    ... including ignored modifiers #{inspect set}"
      XEB.Core.grab_key key: key,
                  modifiers: MapSet.to_list(mods) ++ set,
               owner_events: false,
                grab_window: root,
               pointer_mode: :Async,
              keyboard_mode: :Async
    end
  end

  defp ungrab_chord(%WM.HotKeyDaemon.Chord{key: key, modifiers: mods} = chord,
                    %{root: root} = _state) do
    Logger.debug "WM.HotKeyDaemon.ungrab_chord #{inspect chord}"
    for set <- power_set(ignore_ModMasks()) do
      XEB.Core.ungrab_key key: key,
                    modifiers: MapSet.to_list(mods) ++ set,
                  grab_window: root
    end
  end

  #############################################################################
  # Ignored modifiers

  # Return a list of the ModMask-s of the modifiers that are to be ignored
  defp ignore_ModMasks do
    @ignore_modifiers
    |> Enum.map(&XEB.Mapping.lookup_mod_reverse/1)
    |> Enum.filter(&(&1 != nil))
  end

  # Return the power of a given list. For some reason the function is not
  # part of the standard libraries. The return value is a list.
  defp power_set([]), do: [[]]
  defp power_set([h | rest]) do
    rest_power = power_set rest
    powers = Enum.map rest_power, &([h | &1])
    powers ++ rest_power
  end

  # Return a set of the KeyButMasks of the modifiers that are to be ignored
  defp ignore_KeyButMasks do
    ignore_ModMasks() |> XEB.Mapping.to_KeyButMask
  end
end

