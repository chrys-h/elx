# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule WM.Container.Geometry do
  defstruct [:x, :y, :width, :height, :mapped]
end

defmodule WM.Container do
  @moduledoc ~S"""
  Abstract model for containers. A container can be seen as a frame that has a certain position, size and mapping status (visible or hidden) on the screen, and contains other containers. The bottommost containers do not contain other containers, but instead manage an X window by resizing, moving and mapping/unmapping it to match the bottom-leve container's own status. Each container can move, resize and map or unmap its children in any arbitrary way.
  The Container behaviour abstracts away the details of how containers communicate together and how the container tree is organized. Each callback function has a default definition. The 'default' container defined by these default functions acts as a 'passthrough' that passes every message from its parent to all of its children and every message from any of its children to its parent. Implementors of the beaviour are free to override any, all, or none of these definition to define their own behavior.
  """

  use GenStateMachine
  require Logger

  #############################################################################
  # Public API

  @default_opts %{
    mod: nil,
    children: [],
    parent: nil,
    focus: 0,
    geometry: %WM.Container.Geometry{
      x: 0, y: 0, width: 100, height: 100, mapped: false
    }
  }

  @doc ~S"""
  Starts a `Container` process and links it to the caller process. Possible options for `opts` are:

  * `children:`: the initial list of children. Using this is equivalent to not using it and adding the children one by one. In particular, `handle_add_child` will be called for each one, in the order of the list.
  * `geometry:`: the initial geometry of the container. Using this is not equivalent to changing the geometry after startup if the `parent:` option is used because the parent is set after setting the geometry, and the parent may change the geometry.
  * `focus:`: the focused child, identified by its pid or name or index in the children list.
  * `parent:`: the parent of the container. Using this is equivalent to not using it and add the container as a child to the parent.
  * Every option accepted by GenServer.

  Additional keywords may be present ; they will be ignored.
  """
  def start_link(module, opts \\ []) do
    GenStateMachine.start_link __MODULE__, options_cont(opts, module), options_gen(opts)
  end

  @doc ~S"""
  Starts a `Container` process without any links. See documentation for `start_link` for options.
  """
  def start(module, opts \\ []) do
    GenStateMachine.start __MODULE__, options_cont(opts, module), options_gen(opts)
  end

  # Take the options from the list that are GenServer/GenStateMachine options
  defp options_gen(opts) do
    Enum.filter opts, fn {kw, _} ->
      kw in [:debug, :name, :timeout, :spawn_opt]
    end
  end

  # Take Container-specific options from the list and apply the default values
  defp options_cont(opts, mod) do
    Map.merge %{@default_opts | mod: mod}, Map.new(opts)
  end

  @doc ~S"""
  Stops a `Container` process with the given `reason`.
  """
  def stop(container, reason \\ :normal, timeout \\ :infinity) do
    GenStateMachine.stop container, reason, timeout
  end

  @doc ~S"""
  Returns the given state variable (or the entire `state` if the argument is left to `nil`) synchronously.
  """
  def get_state(container, var \\ nil) do
    GenStateMachine.call container, {:get_state, var}
  end

  @doc ~S"""
  Adds a child to the given `Container` process. The child needs to be a `Container` process too.
  """
  def add_child(container, child) do
    GenStateMachine.cast container, {:add_child, child}
  end

  @doc ~S"""
  Remove a child from a `Container` process. Nothing is done if the `Container` doesn't contain the `child`. The child may be a process (by PID or by name) or an integer that references the index of the child in the `c_children` state variable (which is a list of children).
  """
  def remove_child(container, child) do
    GenStateMachine.cast container, {:rm_child, child}
  end

  @doc ~S"""
  Set the process a `Container` process will report to as its parent. The parent needs to be a `Container` process too. It may be referred to by PID or by name.
  """
  def set_parent(container, new_parent) do
    GenStateMachine.cast container, {:set_parent, new_parent}
  end

  @doc ~S"""
  Sets the on-screen geometry of a `Container` process. This function is meant as a call down the container tree, meaning that the receiving container will change its geometry to fit the arguments of the function without asking its parent. The function is typically called by a parent on its children.
  The Geometry variable is a `WM.Container.Geometry`. Each component may be left to `nil`, in which case it will not be changed (especially the `:mapped` component, event though `nil` could be understand to mean `false`). Integer values supplied may be supplied as literal unsigned integers, in which case they will be substituted for the previous value, or as {:mod, i} where i is a signed integer to be added to the previous value. The `:mapped` component may be `nil` (in which case it is not touched), `true`, `false`, or `:toggle`.
  """
  def set_geometry(container, geometry) do
    GenStateMachine.cast container, {:set_geometry, geometry}
  end

  @doc ~S"""
  Sets the width and height of a `Container`. This function is meant as a call down the container tree, meaning that the receiving container will change its geometry to fit the arguments of the function without asking its parent. The function is typically called by a parent on its children.
  """
  def set_size(container, width, height),
    do: set_geometry container, %WM.Container.Geometry{width: width, height: height}

  @doc ~S"""
  Sets the position of a `Container`, relative to the upper-left corner. This function is meant as a call down the container tree, meaning that the receiving container will change its geometry to fit the arguments of the function without asking its parent. The function is typically called by a parent on its children.
  """
  def set_position(container, x, y),
    do: set_geometry container, %WM.Container.Geometry{x: x, y: y}

  @doc ~S"""
  Sets the mapping status of a `Container`. `true` is visible and `false` is hidden. This function is meant as a call down the container tree, meaning that the receiving container will change its geometry to fit the arguments of the function without asking its parent. The function is typically called by a parent on its children.
  """
  def set_mapping_status(container, mapping),
    do: set_geometry container, %WM.Container.Geometry{mapped: mapping}

  @doc ~S"""
  Causes the given `Container` process to ask its parent to change its geometry to fit the given description. All the given argument substitutions that apply to `set_geometry` alo apply here.
  """
  def hint_geometry(container, geometry) do
    GenStateMachine.cast container, {:hint_geometry, geometry}
  end

  @doc ~S"""
  Causes a `Container` process to ask its parent to change its width and height.
  """
  def hint_size(container, width, height),
    do: hint_geometry container, %WM.Container.Geometry{width: width, height: height}

  @doc ~S"""
  Causes a `Container` process to ask its parent to change its position on screen.
  """
  def hint_position(container, x, y),
    do: hint_geometry container, %WM.Container.Geometry{x: x, y: y}

  @doc ~S"""
  Causes a `Container` process to ask its parent to change its mapping status.
  """
  def hint_mapping_status(container, mapping),
    do: hint_geometry container, %WM.Container.Geometry{mapped: mapping}

  @doc ~S"""
  Used by a child of a `Container` process to ask its parent to change its geometry. This function is meant as a call up the container tree. Both the caller and the target must be `Container` processes and the target must be the parent of the caller. Once the function ha sbeen called, the target (the parent of the caller) is free to ignore, moify or act upon the message. It may lead to the parent calling `set_geometry/6` on the child. The parent may also call this function on its own parent.
  All variable substitutions that apply for `set_geometry` also apply here.
  """
  def order_geometry(parent, geometry) do
    GenStateMachine.cast parent, {:order_geometry, geometry, self()}
  end

  @doc ~S"""
  Used by a child of a `Container` process to ask its parent to change its width and height.
  """
  def order_size(parent, width, height),
    do: order_geometry parent, %WM.Container.Geometry{width: width, height: height}

  @doc ~S"""
  Used by a child of a `Container` process to ask its parent to change its on-screen position.
  """
  def order_position(parent, x, y),
    do: order_geometry parent, %WM.Container.Geometry{x: x, y: y}

  @doc ~S"""
  Used by a child of a `Container` process to ask its parent to change its mapping status.
  """
  def order_mapping_status(parent, mapping),
    do: order_geometry parent, %WM.Container.Geometry{mapped: mapping}

  @doc ~S"""
  Sets the focus on the given `Container` process. This function is meant as a call down the container tree, meaning that it is normally called by a parent on one of its children. The receiving process will take the focus without asking its parent.
  """
  def set_focus(container) do
    GenStateMachine.cast container, :set_focus
  end

  @doc ~S"""
  Causes the given `Container` to ask its parent to set the focus on it.
  """
  def hint_focus(container) do
    GenStateMachine.cast container, :hint_focus
  end

  @doc ~S"""
  Used by a child of a `Container` process to ask its parent to set the focus on it. This function si meant as a call up the container tree, and the parent may react to the call in any way or not react at all.
  """
  def order_focus(parent) do
    GenStateMachine.cast parent, {:order_focus, self()}
  end

  @doc ~S"""
  Causes a `Container` process to set the focus on one of its children. By default, the recognized positions specifiers are `:next` and `:previous`, causing the container to shift the focus along the list of children, and any integer `n`, causing the container to set the focus on its `n`th element. Any other specification of focus will, by default, result in a call of this function on the container's own parent with the same direction. Implementors of the behaviour are free to override any of these behaviors and define their own possible specifications, such as `:up`, `:down`, `:right`, `:left`, etc.
  """
  def move_focus(container, position) do
    GenStateMachine.cast container, {:move_focus, position}
  end

  # TODO: Reordering of children?

  # Used by a Container in the container tree when it terminates to notify its parent and children.
  defp c_notify_terminate(container) do
    GenStateMachine.cast container, {:terminates, self()}
  end

  #############################################################################
  # Types

  @type geom_mod_int :: {:mod, integer} | non_neg_integer | nil

  @type geom_mod_bool :: true | false | :toggle | nil

  @type geometry_modification :: %WM.Container.Geometry{
    x: geom_mod_int, y: geom_mod_int,
    width: geom_mod_int, height: geom_mod_int,
    mapped: geom_mod_bool
  }

  @type static_geometry :: %WM.Container.Geometry{
    x: non_neg_integer, y: non_neg_integer,
    width: non_neg_integer, height: non_neg_integer,
    mapped: boolean
  }

  @type container :: pid

  @type state :: %{
    required(:c_module) => module,
    required(:c_parent) => container,
    required(:c_children) => [container],
    required(:c_geometry) => static_geometry,
    required(:c_focused_child) => non_neg_integer
  }

  #############################################################################
  # Module callbacks

  @doc ~S"""
  Invoked on Container initialization.
  """
  @callback init(args :: term, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked on Container termination.
  """
  @callback terminate(reason :: term, state) ::
    term

  @doc ~S"""
  Invoked when receiving any message that is not part of the specification.
  Possible return values are:

     `{:ok, state}`    To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_info(message :: term, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked before a child is added. The `child` variable is the pid of a Container process. Note that the child doesn't need to be added to the children list ; that is done by default. By default, the child is added at the end of the list but the implementor may add it at any other position instead by altering the `c_children` state variable.
  Possible return values are :

     `{:ok, state}`    To carry on with the child addition, with the new state.

     `{:deny, state}`  To cancel the child addition and resume operation with the
                       new state. In that case, the child is not added to the
                       list of children.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_add_child(new_child :: container, state) ::
    {:ok, state} |
    {:deny, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked before a child is removed. The child doesn't need to be removed from the childrens list. That is done automatically by default. The child argument is the index of the child that is to be removed in the children list.
  Possible return values are:

     `{:ok, state}`    To carry on with the child removal, with the new state.

     `{:stop, reason}` To terminate the Container process with the given reason.

  The child is given by its index in the `c_children` state variable.
  """
  @callback handle_rem_child(child :: non_neg_integer, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked when the parent of the Container is changed. The `parent` state variable doesn't need to be changed ; that is done automatically.
  Possible return values are:

     `{:ok, state}`    To carry on with the readoption.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_parent_change(new_parent :: container, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a `set_geometry` request. The default behavior is to change the geometry variables and propagate the geometry change to children ; it can be overriden by implementing the callback.
  Note taht the geometry passed as the first argument is a static geometrey, meaning that it is to be copied in-place as the new geometry. All variable substitutions (`nil`s, `{:mod, n}`, etc.) have already been applied.
  The changing of the `c_geometry` state variable is done automatically.
  Possible return values are:

     `{:ok, state}`    To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_set_geometry(static_geometry, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a `hint_geometry` request. The default behaviour is to issue a geometry order to the parent Container. It can be overriden by implementing the callback.
  Possible return values are:

     `{:ok, state}`    To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_geometry_hint(geometry_modification, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a `order_geometry` request. The default behaviour is to propagate the request to the parent Container. It can be overriden by implementing the callback.
  The `from` variable is the child that sent the request, identified by its index in the children list.
  Possible return values are:

     `{:ok, state}     To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_geometry_order(from :: non_neg_integer, geometry_modification, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a `set_focus` request. The default behavior is to propagate the request to the child currently held as the focus of the container. It can be overriden by implementing the callback.
  Possible return values are:

     `{:ok, state}     To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_set_focus(state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a `focus_hint` request. The default behavior is to issue a focus order to the parent. It can be overriden by implementing the callback.
  Possible return values are:

     `{:ok, state}     To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_focus_hint(state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a `focus_order` request. The default behavior is to propagate the request to the parent Container and set the container's focus on the caller. It can be overriden by implementing the callback. The `from` variable is the child that sent the request, identified by its index in the children list.
  Possible return values are:

     `{:ok, state}     To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_focus_order(from :: integer, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  @doc ~S"""
  Invoked upon receiving a move_focus request. The default behavior is detailed in the documentation for the `move_focus` function. It can be overriden by implementing the callback.
  Possible return values are:

     `{:ok, state}     To resume normal operation with the new state.

     `{:stop, reason}` To terminate the Container process.
  """
  @callback handle_move_focus(move :: term, state) ::
    {:ok, state} |
    {:stop, reason :: term}

  #############################################################################
  # Default callbacks

  defmacro __using__(_) do
    quote location: :keep do
      @behaviour WM.Container

      @doc false
      def init(_options, state), do: {:ok, state}

      @doc false
      def terminate(reason, _state), do: reason

      @doc false
      def handle_info(_message, state), do: {:ok, state}

      @doc false
      def handle_add_child(_new_child, state), do: {:ok, state}

      @doc false
      def handle_rem_child(_child, state), do: {:ok, state}

      @doc false
      def handle_parent_change(_new_parent, state), do: {:ok, state}

      @doc false
      def handle_set_geometry(new_geometry, %{c_children: children} = state) do
        Enum.map children, &WM.Container.set_geometry(&1, new_geometry)
        {:ok, %{state | c_geometry: new_geometry}}
      end

      @doc false
      def handle_geometry_hint(new_geometry, %{c_parent: parent} = state) do
        WM.Container.order_geometry parent, new_geometry
        {:ok, state}
      end

      @doc false
      def handle_geometry_order(_from, new_geometry, %{c_parent: parent} = state) do
        WM.Container.order_geometry parent, new_geometry
        {:ok, state}
      end

      @doc false
      def handle_set_focus(%{c_children: children, c_focused_child: index} = state) do
        WM.Container.set_focus Enum.at(children, index)
        {:ok, state}
      end

      @doc false
      def handle_focus_hint(%{c_parent: parent} = state) do
        WM.Container.order_focus parent
        {:ok, state}
      end

      @doc false
      def handle_focus_order(from, %{c_parent: parent} = state) do
        WM.Container.order_focus parent
        {:ok, %{state | c_focused_child: from}}
      end

      @doc false
      def handle_move_focus(n, %{c_children: children} = state)
                           when is_integer(n) and n < length(children) do
        WM.Container.set_focus Enum.at(children, n)
        {:ok, %{state | c_focused_child: n}}
      end

      @doc false
      def handle_move_focus(:previous, %{c_children: children, c_focused_child: index} = state) do
        new_index = Integer.mod(index - 1, length(children))
        WM.Container.set_focus Enum.at(children, new_index)
        {:ok, %{state | c_focused_child: new_index}}
      end

      @doc false
      def handle_move_focus(:next, %{c_children: children, c_focused_child: index} = state) do
        new_index = Integer.mod(index + 1, length(children))
        WM.Container.set_focus Enum.at(children, new_index)
        {:ok, %{state | c_focused_child: new_index}}
      end

      @doc false
      def handle_move_focus(move, %{c_parent: nil} = state) do
        {:ok, state}
      end

      @doc false
      def handle_move_focus(move, %{c_parent: parent} = state) do
        WM.Container.move_focus parent, move
        {:ok, state}
      end

      defoverridable [init: 2, terminate: 2, handle_info: 2,
                      handle_add_child: 2, handle_rem_child: 2, handle_parent_change: 2,
                      handle_set_geometry: 2, handle_geometry_hint: 2, handle_geometry_order: 3,
                      handle_set_focus: 1, handle_focus_hint: 1, handle_focus_order: 2,
                      handle_move_focus: 2]
    end
  end

  #############################################################################
  # GenStateMachine callbacks

  # Basic container state. Implementors of the behaviour may add their own variables
  # but the basic variables need to be in the map at all times.
  # All variables required by the Container behaviour have names that start with c_
  # to avoid name clashes.
  @basic_cont_state %{
    # Behaviour implementer
    c_module: nil,

    # Position in the container hierarchy
    c_parent: nil,
    c_children: [],

    # Geometry
    c_geometry: %WM.Container.Geometry{
      x: 0, y: 0, width: 100, height: 100, mapped: false
    },

    # Focused child, as an index in the list of children.
    c_focused_child: 0
  }

  @doc false
  def init(%{mod: mod} = args) do
    init_state = %{@basic_cont_state | c_module: mod}
    with {:ok, st} <- mod.init(args, init_state),
         {:ok, st} <- c_state_reduce(args.children, st, &c_adopt_child(&1, &2)),
         {:ok, st} <- c_move_focus(args.focus, st),
         {:ok, st} <- c_set_geometry(args.geometry, st),
         {:ok, st} <- c_adopt_parent(args.parent, st),
         do: {:ok, :none, st}
  end

  @doc false
  def terminate(reason, _, %{c_parent: par, c_children: chn, c_module: mod} = data) do
    Enum.map [par | chn], &c_notify_terminate/1
    mod.terminate reason, data
  end

  @doc false
  def handle_event({:call, from}, {:get_state, nil}, _, data),
    do: {:keep_state_and_data, {:reply, from, data}}
  def handle_event({:call, from}, {:get_state, var}, _, data),
    do: {:keep_state_and_data, {:reply, from, Map.get(data, var)}}

  def handle_event(:cast, {:add_child, new_child}, _, data),
    do: c_check c_adopt_child(new_child, data)
  def handle_event(:cast, {:rm_child, child}, _, data),
    do: c_check c_remove_child(child, data)
  def handle_event(:cast, {:set_parent, new_parent}, _, data),
    do: c_check c_adopt_parent(new_parent, data)

  def handle_event(:cast, {:set_geometry, new_geom}, _, data),
    do: c_check c_set_geometry(new_geom, data)
  def handle_event(:cast, {:hint_geometry, new_geom}, _, data),
    do: c_check c_hint_geometry(new_geom, data)
  def handle_event(:cast, {:order_geometry, new_geom, orderer}, _, data),
    do: c_check c_order_geometry(new_geom, orderer, data)

  def handle_event(:cast, :set_focus, _, data),
    do: c_check c_set_focus(data)
  def handle_event(:cast, :hint_focus, _, data),
    do: c_check c_hint_focus(data)
  def handle_event(:cast, {:order_focus, orderer}, _, data),
    do: c_check c_order_focus(orderer, data)
  def handle_event(:cast, {:move_focus, move}, _, data),
    do: c_check c_move_focus(move, data)

  def handle_event(:cast, {:terminates, cont}, _, data),
    do: c_check c_handle_terminate_notify(cont, data)

  def handle_event(:info, info, _, data),
    do: c_check c_handle_info(info, data)

  #############################################################################
  # Internals

  # To avoid name clashes, all internal function names start with c_

  # This function checks the output of an internal function and replaces
  # the generic {:ok, ...} output with GenStateMachine-compatible return values
  defp c_check({:ok, new_state}), do: {:keep_state, new_state}
  defp c_check({:stop, _} = stop), do: :stop

  # Similar to Enum.reduce, except that the tag in the return value of the
  # callback is taken into account: if it is {:ok, state}, the function
  # continues with `state` as its accumulator. Otherwise, the entire return
  # value is returned immediately (without finishing the reduce operation)
  defp c_state_reduce([], state, _), do: {:ok, state}
  defp c_state_reduce([head | rst], state, fun) do
    case fun.(head, state) do
      {:ok, new_state} -> c_state_reduce(rst, new_state, fun)
      ret -> ret
    end
  end

  defp c_resolve_proc(nil), do: nil
  defp c_resolve_proc(n) when is_atom(n), do: Process.whereis n
  defp c_resolve_proc(p) when is_pid(p), do: p
  defp c_resolve_proc(_), do: :error

  defp c_apply_bool_modif(base, :toggle), do: not base
  defp c_apply_bool_modif(base, nil), do: base
  defp c_apply_bool_modif(_, modif) when is_boolean(modif),
    do: modif

  defp c_apply_integer_modif(base, nil), do: base
  defp c_apply_integer_modif(base, {:mod, x}), do: base + x
  defp c_apply_integer_modif(_, modif) when is_integer(modif),
    do: modif

  defp c_apply_geom_modif(base, modif) do
    %WM.Container.Geometry{
      x: c_apply_integer_modif(base.x, modif.x),
      y: c_apply_integer_modif(base.y, modif.y),
      width: c_apply_integer_modif(base.width, modif.width),
      height: c_apply_integer_modif(base.height, modif.height),
      mapped: c_apply_bool_modif(base.mapped, modif.mapped)
    }
  end

  defp c_child_index(child, %{c_children: chn}) when is_integer(child) do
    if child >= 0 and child < length(chn) do
      child
    else
      nil
    end
  end

  defp c_child_index(child, %{c_children: chn}) do
    pid = c_resolve_proc child
    Enum.find_index(chn, &(&1 == pid))
  end

  # Adopt a new child by invoking the module callback and doing mutual adoption.
  # Returns `{:ok, new_state}` or `{:stop, reason}`
  defp c_adopt_child(new_child, %{c_children: children, c_parent: parent} = state) do
    nc = c_resolve_proc(new_child)
    cond do
      nc == nil -> {:ok, state}
      nc == :error -> {:ok, state}
      nc == self() -> {:ok, state}
      nc == parent -> {:ok, state}
      nc in children -> {:ok, state}

      true -> c_do_adopt_child(nc, state)
    end
  end

  defp c_do_adopt_child(nc, %{c_module: mod} = state) do
    case mod.handle_add_child(nc, state) do
      {:deny, new_state} -> {:ok, new_state}
      {:ok, new_state} -> c_finalize_add_child nc, new_state
      {:stop, _} = ret -> ret
    end
  end

  defp c_finalize_add_child(nc, %{c_children: children} = state) do
    final_state = if nc in children
      do state
      else %{state | c_children: List.insert_at(children, -1, nc)}
    end

    set_parent nc, self()
    {:ok, final_state}
  end

  defp c_remove_child(child, state) do
    case c_resolve_proc child do
      nil -> {:ok, state}
      :error -> {:ok, state}

      pid ->
        case c_child_index(pid, state) do
          nil -> {:ok, state}
          i -> c_do_remove_child i, pid, state
        end
    end
  end

  defp c_do_remove_child(index, pid, %{c_module: mod} = state) do
    case mod.handle_rem_child(index, state) do
      {:stop, _} = ret -> ret
      {:ok, new_state} -> c_finalize_remove_child pid, new_state
    end
  end

  defp c_finalize_remove_child(pid, %{c_children: chn} = state) do
    final_state = if pid in chn do
      {:ok, refocused_state} = c_move_focus(:prev, state)
      %{refocused_state | c_children: List.delete(chn, pid)}
    else
      state
    end

    set_parent pid, nil

    {:ok, final_state}
  end

  defp c_adopt_parent(new_parent, %{c_parent: parent, c_children: children} = state) do
    np = c_resolve_proc new_parent
    cond do
      np == :error -> {:ok, state}
      np == self() -> {:ok, state}  # HACK: Is this kind of ostrich policy a good idea?
      np == parent -> {:ok, state}  # Maybe an error should be reported ?
      np in children -> {:ok, state}

      true -> c_do_adopt_parent(np, state)
    end
  end

  defp c_do_adopt_parent(np, %{c_parent: old_parent, c_module: mod} = state) do
    case mod.handle_parent_change(np, state) do
      {:ok, new_state} -> c_finalize_adopt_parent(np, old_parent, new_state)
      {:stop, _} = ret -> ret
    end
  end

  defp c_finalize_adopt_parent(new, old, state) do
    remove_child old, self()
    add_child new, self()
    final_state = Map.put state, :c_parent, new
    {:ok, final_state}
  end

  defp c_set_geometry(geom_modif, %{c_module: mod, c_geometry: curr_geom} = state) do
    final_geom = c_apply_geom_modif curr_geom, geom_modif
    with {:ok, new_state} <- mod.handle_set_geometry(final_geom, state),
         do: {:ok, %{new_state | c_geometry: final_geom}}
  end

  defp c_hint_geometry(geom, %{c_module: mod} = state) do
    mod.handle_geometry_hint(geom, state)
  end

  defp c_order_geometry(geom, orderer, %{c_module: mod} = state) do
    case c_child_index(orderer, state) do
      nil -> {:ok, state}
      i -> mod.handle_geometry_order(i, geom, state)
    end
  end

  defp c_set_focus(%{c_module: mod} = state) do
    mod.handle_set_focus(state)
  end

  defp c_hint_focus(%{c_module: mod} = state) do
    mod.handle_focus_hint(state)
  end

  defp c_order_focus(orderer, %{c_module: mod} = state) do
    case c_child_index(orderer, state) do
      nil -> {:ok, state}
      i -> mod.handle_focus_order(i, state)
    end
  end

  defp c_move_focus(move, %{c_module: mod} = state) do
    mod.handle_move_focus(move, state)
  end

  defp c_handle_terminate_notify(cont, %{c_children: chn, c_parent: par} = state) do
    cond do
      cont in chn -> c_handle_child_term(cont, state)
      cont == par -> c_handle_parent_term(state)
      true -> {:ok, state}
    end
  end

  defp c_handle_child_term(child, state) do
    c_remove_child child, state
  end

  defp c_handle_parent_term(_state) do
    {:stop, :normal}
  end

  defp c_handle_info(info, %{c_module: mod} = state) do
    mod.handle_info(info, state)
  end
end

