# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule UserConfig do
  @moduledoc ~S"""
  This module defines a process that loads a user-defined module from the file with the given path. The process itself is an event-driven state machine that runs specified code on specified occurences (e.g. on initialization and terminaton, when a certain hotkey is struck, when a something happens to a certain container, when the X server issues a given event, etc.). Those events-responses are defined in the user-written module.
  If more than one module is defined in the file, the one defined last is used.
  The code of the responses is like the code of any normal Elixir function. A `state` variable is threaded through those functions ; it contains a map containing arbitrary keys, with a few predefined ones.
  """

  use GenStateMachine
  require Logger

  #############################################################################
  # Public API

  @doc ~S"""
  Starts a `UserConfig` process and links it to the caller process.
  """
  def start_link(name \\ nil, path) do
    GenStateMachine.start_link __MODULE__, %{path: path}, name: name
  end

  @doc ~S"""
  Starts a `UserConfig` process without any links. The options are the same as for `start_link`.
  """
  def start(name \\ nil, path) do
    GenStateMachine.start __MODULE__, %{path: path}, name: name
  end

  @doc ~S"""
  Stops a `UserConfig` process with the given `reason`.
  """
  def stop(pid \\ UserConfig, reason \\ :normal, timeout \\ :infinity) do
    GenStateMachine.stop pid, reason, timeout
  end

  @doc ~S"""
  Notify the UserConfig process of a hotkey. This function is used by the Hotkey daemon.
  """
  def notify_hotkey(pid \\ UserConfig, hk_id) do
    GenStateMachine.cast pid, {:notify_hotkey, hk_id}
  end

  #############################################################################
  # GenStateMachine callbacks

  @basic_state %{
    # The user-defined module
    uc_module: nil,

    # The user-defined hooks
    uc_hooks: %{},

    # The user-defined hotkeys
    # NOTE: The map does NOT match hotkeys to actions. Instead, each hotkey
    # has an ID, and the map matches each hotkey's ID to the {hotkey, action}
    # couple. This is because the hotkey daemon is made to send the ID of
    # each hotkey to the UserConfig process. IDs are arbitrarily assigned
    # to hotkeys for this purpose.
    uc_hotkeys: %{},

    # The state variable exposed to the user-defined code
    uc_state: %{}
  }

  @doc false
  def init(%{path: path} = args) do
    Logger.debug "UserConfig initialization..."
    # Read the file
    {uc_module, _} = path |> Code.load_file |> List.last

    # Initialization
    state = %{@basic_state | uc_module: uc_module}
    {:ok, new_uc_state} = uc_module.enter state.uc_state
    state = %{state | uc_state: new_uc_state}

    # Setup hooks - TODO

    # Assign hotkeys ID
    hotkeys = uc_module.keys()
    |> Enum.with_index
    |> Map.new(fn {key, index} -> {index, key} end)
    Logger.debug "Hotkeys: #{inspect hotkeys}"
    state = %{state | uc_hotkeys: hotkeys}

    # Register them with hotkey daemon.
    me = self()
    hotkeys
    |> Enum.map(fn {index, {hk, _action}} ->
      {hk, fn -> UserConfig.notify_hotkey(me, index) end}
    end)
    |> WM.HotKeyDaemon.add_hotkeys

    # Successful function run
    {:ok, :none, state}
  end

  @doc false
  def terminate(reason, _, %{uc_module: mod, uc_state: uc_state} = data) do
    mod.exit uc_state
  end

  @doc false
  def handle_event(:cast, {:notify_hotkey, id}, _, %{uc_hotkeys: hks} = state) do
    case Map.fetch(hks, id) do
      :error ->
        Logger.warn "Hotkey notification no. #{inspect id} : no such hotkey"
        :keep_state_and_data

      {:ok, {_, action}} ->
        run_action action, state
    end
  end

  #############################################################################
  # Internals

  defp run_action(f, args \\ [], state) do
    case do_run_action(f, args, state) do
      {:ok, new_state} ->
        {:keep_state, %{state | uc_state: new_state}}

      :stop ->
        :stop

      # Anything other than {:ok, state} or :stop means the user has not
      # specified a return value. In that case, we leave everything be.
      _ ->
        :keep_state_and_data
    end
  end

  defp do_run_action(f, args, state) do
    try do
      apply(f, args ++ [state.uc_state])
    rescue
      FunctionClauseError -> {:ok, state}
    end
  end
end

