# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule UserConfig.Config.Callback do
  @moduledoc false

  @doc false
  defmacro __before_compile__(env) do
    keys = Module.get_attribute(env.module, :keys)
    hooks = Module.get_attribute(env.module, :hooks)

    quote do
      def keys, do: unquote(keys)
      def hooks, do: unquote(hooks)
    end
  end
end

defmodule UserConfig.Config do
  @moduledoc ~S"""
  Userconfig functions and default implementation.
  """

  require Logger

  #############################################################################
  # Public API

  defmacro __using__(_) do
    quote do
      require UserConfig.Config
      import UserConfig.Config
      import UserConfig.DSL

      @before_compile UserConfig.Config.Callback

      Module.register_attribute __MODULE__, :keys, accumulate: true
      Module.register_attribute __MODULE__, :hooks, accumulate: true

      @doc false
      def enter(state) do
        {:ok, state}
      end

      @doc false
      def exit(state) do
        {:ok, state}
      end

      defoverridable [enter: 1, exit: 1]
    end
  end

  @doc ~S"""
  Define a hotkey.
  """
  defmacro key(stroke, args \\ quote do _ end, do: body) do
    hk = decode_hotkey stroke
    Logger.debug "Decoding successful: #{inspect hk}"
    fun = build_fn [args], body

    quote do
      @keys {unquote(Macro.escape(Macro.escape(hk))), unquote(Macro.escape(fun))}
    end
  end

  @doc ~S"""
  Define a hook.
  """
  defmacro hook({name, _, args}, do: body) do
    fun = build_fn(args, body)

    quote do
      @hooks {unquote(name), unquote(fun)}
    end
  end

  #############################################################################
  # Internals

  defp build_fn(args, body) do
    quote do
      fn unquote_splicing(args) ->
        unquote(body)
      end
    end
  end

  defp decode_hotkey(stroke),
    do: stroke |> parse_hotkey |> to_hkd_struct

  defp parse_hotkey({:>, _, [lhs, rhs]}) do
    # The RHS is necessarily a chord. The LHS may be a chord or a sequence
    # (because the > operator is left-associative)
    parsed_rhs = parse_hotkey rhs
    case parse_hotkey(lhs) do
      %WM.HotKeyDaemon.Sequence{chords: cds} ->
        %WM.HotKeyDaemon.Sequence{chords: cds ++ [parsed_rhs]}

      %WM.HotKeyDaemon.Chord{} = cd ->
        %WM.HotKeyDaemon.Sequence{chords: [cd, parsed_rhs]}
    end
  end

  defp parse_hotkey({:+, _, [lhs, rhs]}) do
    combine_chords(parse_hotkey(lhs), parse_hotkey(rhs))
  end

  defp parse_hotkey({:__aliases__, _, [x]}) when is_atom(x) do
    parse_hotkey x
  end

  defp parse_hotkey({x, _, _}) when is_atom(x) do
    parse_hotkey x
  end

  defp parse_hotkey(x) when is_atom(x) do
    # Figure out of the atom is a modifier or not and embed it in a Chord
    case XEB.Mapping.lookup_mod_reverse(x) do
      nil -> %WM.HotKeyDaemon.Chord{key: x, modifiers: []}
      _ -> %WM.HotKeyDaemon.Chord{key: nil, modifiers: [x]}
    end
  end

  defp parse_hotkey(x) do
    raise "Invalid hotkey expression: #{inspect x}"
  end

  # HACK: This should be in WM.HotKeyDaemon
  defp combine_chords(%WM.HotKeyDaemon.Chord{key: key1, modifiers: mods1},
                      %WM.HotKeyDaemon.Chord{key: key2, modifiers: mods2}) do
    unless key1 == nil or key2 == nil,
      do: raise "Hotkey error: only one non-modifier key is allowed by chord"

    %WM.HotKeyDaemon.Chord{
      modifiers: mods1 ++ mods2,
      key: key1 || key2
    }
  end

  defp to_hkd_struct(%WM.HotKeyDaemon.Sequence{} = x),
    do: x
  defp to_hkd_struct(%WM.HotKeyDaemon.Chord{} = x),
    do: %WM.HotKeyDaemon.Sequence{chords: [x]}
  defp to_hkd_struct(x),
    do: raise "Invalid to_hkd_struct input: #{inspect x}"
end

